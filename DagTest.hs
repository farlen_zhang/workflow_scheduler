import Data.List (nub, sort)
import Data.Graph (Graph, Vertex, buildG, components, reachable)
import Test.HUnit
import Dag (isConnected, nodesByLevel, nodesByMaxLevel)

{-
*Main> runTestTT $ TestList [isConnectedTests, nodesByLevelTests, nodesByMaxLevelTests]
Cases: 14  Tried: 14  Errors: 0  Failures: 0
Counts {cases = 14, tried = 14, errors = 0, failures = 0}
-}

main :: IO Counts
main = runTestTT $ TestList [isConnectedTests, nodesByLevelTests, nodesByMaxLevelTests]

notConnected = buildG (1,4) [(1,2), (1,3), (3,2)]
cyc = buildG (1,3) [(1,2), (2,3), (3,1)]
line = buildG (1,3) [(1,2), (2,3)]
dag = buildG (1,3) [(1,2), (2,3), (1,3)]
dag1 = buildG (1,7) [(1,2), (1,3), (2,4), (2,5), (3,6), (4,7), (5,7), (6,5), (6,7)]
dag2 = buildG (1,5) [(1,3), (2,3), (2,5), (3,4), (5,4)]
mroots = buildG (1,7) [(1,2), (2,3), (2,4), (2,5), (3,5), (4,5), (5,6), (7,4)]
mroots1 = buildG (1,8) [(1,4), (1,5), (2,4), (3,5), (3,8), (4,6), (4,7), (4,8), (5,7)]
tree = buildG (1,5) [(1,2), (1,3), (3,4), (3,5)]
tree1 = buildG (1,9) [(1,3), (1,4), (2,4), (2,5), (4,6), (4,7), (7,9), (8,9)]

isConnectedTests = TestLabel "isConnected tests" $ TestList [notcon, dagcon1, dagcon2, cyclecon, treecon]

notcon = TestCase (assertBool "isConnected not connected graph" ((not . isConnected) notConnected))
dagcon1 = TestCase (assertBool "isConnected dag" (isConnected dag))
dagcon2 = TestCase (assertBool "isConnected dag with mutiple starting points" (isConnected mroots))
cyclecon = TestCase (assertBool "isConnected cycle" (isConnected cyc))
treecon = TestCase (assertBool "isConnected tree" (isConnected tree1))

nodesByLevelTests = TestLabel "nodesByLevel tests" $ TestList [test1, test2, test3, test4, test5, test6, test7]

test1 = TestCase (assertEqual "nodesByLevel line"
  [[1],[2],[3]]
  (map sort (nodesByLevel line [1])))

test2 = TestCase (assertEqual "nodesByLevel dag"
  [[1],[2,3],[3]]
  (map sort (nodesByLevel dag [1])))

test3 = TestCase (assertEqual "nodesByLevel dag1"
  [[1],[2,3],[4,5,6],[5,7,7,7],[7]]
  (map sort (nodesByLevel dag1 [1])))

test4 = TestCase (assertEqual "nodesByLevel mroots"
  [[1,7],[2,4],[3,4,5,5],[5,5,6,6],[6,6]]
  (map sort (nodesByLevel mroots [1,7])))

test5 = TestCase (assertEqual "nodesByLevel mroots1"
  [[1,2,3],[4,4,5,5,8],[6,6,7,7,7,7,8,8]]
  (map sort (nodesByLevel mroots1 [1,2,3])))

test6 = TestCase (assertEqual "nodesByLevel tree"
  [[1],[2,3],[4,5]]
  (map sort (nodesByLevel tree [1])))

test7 = TestCase (assertEqual "nodesByLevel tree1"
  [[1,2,8],[3,4,4,5,9],[6,6,7,7],[9,9]]
  (map sort (nodesByLevel tree1 [1,2,8])))

nodesByMaxLevelTests = TestLabel "nodesByMaxLevel tests" $ TestList [test8, test9, test10, test11, test12, test13, test14]

test8 = TestCase (assertEqual "nodesByMaxLevel line"
  [[1],[2],[3]]
  (map sort (nodesByMaxLevel line [1])))

test9 = TestCase (assertEqual "nodesByMaxLevel dag"
  [[1],[2],[3]]
  (map sort (nodesByMaxLevel dag [1])))

test10 = TestCase (assertEqual "nodesByMaxLevel dag1"
  [[1],[2,3],[4,6],[5],[7]]
  (map sort (nodesByMaxLevel dag1 [1])))

test11 = TestCase (assertEqual "nodesByMaxLevel mroots"
  [[1,7],[2],[3,4],[5],[6]]
  (map sort (nodesByMaxLevel mroots [1,7])))

test12 = TestCase (assertEqual "nodesByMaxLevel mroots1"
  [[1,2,3],[4,5],[6,7,8]]
  (map sort (nodesByMaxLevel mroots1 [1,2,3])))

test13 = TestCase (assertEqual "nodesByMaxLevel tree"
  [[1],[2,3],[4,5]]
  (map sort (nodesByMaxLevel tree [1])))

test14 = TestCase (assertEqual "nodesByMaxLevel tree1"
  [[1,2,8],[3,4,5],[6,7],[9]]
  (map sort (nodesByMaxLevel tree1 [1,2,8])))
