{-# LANGUAGE DeriveGeneric #-}

module Parser where

import Data.Aeson (FromJSON, decode)
import Data.Graph (Bounds, Edge, Graph, Vertex, buildG)
import GHC.Generics (Generic)

import qualified Data.ByteString.Lazy as B (ByteString)
import qualified Data.Set as S (Set, size, empty, insert, null, fromList, (\\))

type CommandList = [String]

data Task        = Task { tid       :: Int
                        , dependsOn :: [Int]
                        , command   :: String
                        }
  deriving (Generic,Show)

instance FromJSON Task


buildGraph  :: B.ByteString -> (Graph, [Vertex], CommandList)
buildGraph s = case decode s of         -- decode :: FromJSON a => ByteString -> Maybe a
  Nothing   -> error "Cannot parse JSON."
  Just ts   -> buildGraphFromTasks ts


buildGraphFromTasks   :: [Task] -> (Graph, [Vertex], CommandList)
buildGraphFromTasks [] = error "Empty graph."
buildGraphFromTasks ts =
  let ids             :: [Vertex]
      ids              = map tid ts
      bounds          :: Bounds    
      bounds           = (minimum ids, maximum ids)
      edges           :: [Edge]    
      edges            = concatMap (\to -> map (\from -> (from, tid to)) (dependsOn to)) ts
      startingTids    :: [Vertex]
      startingTids     = (map tid . filter (null . dependsOn)) ts
  in case and [checkVertices bounds ids, checkEdges edges ids] of
    True              -> (buildG bounds edges, startingTids, map command ts)
    otherwise         -> error "Impossible!"

-- -- record syntax
-- type Bounds = (Vertex, Vertex)
-- type Edge = (Vertex, Vertex)
      -- 对 dependsOn 里的每个 tid, 即 graph 中一个 edge 的起点， 和这个 tid 本身组成一个 pair, 来表示一个 edge
      -- apply function (\c -> (c, tid t)) to each tid in dependsOn of a task, tid t is the tid of current task
-- buildG :: Data.Graph.Bounds ->     [Data.Graph.Edge] -> Graph

-- check tid: make sure tids start from 1 and are continuous


checkVertices           :: Bounds -> [Vertex] -> Bool
checkVertices (mi, ma) tids = if and [mi == 1, ma == length tids, (S.size . S.fromList) tids == length tids]
  then True
  else error "Task ids must start from 1 and be continuous."

--     n is number of tasks

-- check vertices in dependsOn: check if there's any vertices involved in edges but not a vertex of the graph


checkEdges               :: [Edge] -> [Vertex] -> Bool
checkEdges edges vertices =
  let
    verticesFromEdges    :: S.Set Vertex
    verticesFromEdges     = foldl (\accum (v1, v2) -> S.insert v2 (S.insert v1 accum)) S.empty edges
    unknownVertices       = verticesFromEdges S.\\ S.fromList vertices
  in
    if S.null unknownVertices
    then True
    else error ("Unknown vertices found in edges: " ++ show unknownVertices)
    

    -- a set of vertices
    -- 把 edges 里的每个 vertex 都加到一个 set 里
    -- -- S.fromList 把一个list convert 成一个s    et
    -- (S.\\) :: Ord a => S.Set a -> S.Set a -> S.Set a
    -- 从 verticesFromEdges 中移除 vertices 中的 vertices, 即找出 verticesFromEdges 中出现，但没有在 tid 中的vertices
    -- -- S.null :: S.Set a -> Bool
