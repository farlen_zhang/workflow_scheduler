> module Dag where

> import Data.Graph (Graph, Vertex, buildG, components, reachable)
> import Data.Array ((!), assocs)
> import Data.List (nub)
> import System.Process (spawnCommand, waitForProcess)
> import qualified Control.Monad.Parallel as P (mapM)
> import qualified Data.Set as Set (Set, empty, member, union, fromList)

----------------------------------------------------------------------------------------
some example graphs
----------------------------------------------------------------------------------------
              notConnected
                 1         4
                |  \
                |   2
                |  /
                3  

               dag
                 1
                |  \
                |   2
                |  /
                3  

              cycle
               1
              |  \
              |   2
              |  /
               3   

                 mroots
                    1
                    |
               7    2
                \ / | \
                  4 |  3
                  \ | /
                6 - 5

type Graph = Table [Vertex]
type Table a = Array Vertex a
so a Graph is Array Vertex [Vertex], 所以一个Graph就是a table of 每个vertex和与这个vertex有edge直接相连的list of vertex

> notConnected = buildG (1,4) [(1,2), (1,3), (3,2)]
> dag = buildG (1,3) [(1,2), (2,3), (1,3)]
> cyc = buildG (1,3) [(1,2), (2,3), (3,1)]
> line = buildG (1,3) [(1,2), (2,3)]  -- direct & indirect reachable
> mroots = buildG (1,7) [(1,2), (2,3), (2,4), (2,5), (3,5), (4,5), (5,6), (7,4)]

TODO: failure handling

----------------------------------------------------------------------------------------
excec: 1. check isConnected  2. check isDAG  3. nodesByMaxLevel   4. commandsByMaxLevel
----------------------------------------------------------------------------------------

> type CommandList = [String]

> exec :: Graph -> [Vertex] -> CommandList -> IO ()
> exec gr starters cmds =
>   let
>     cmdsByLevel :: [CommandList]
>     cmdsByLevel  = (commandsByMaxLevel cmds . nodesByMaxLevel gr) starters
>   in
>     if isConnected gr && isDAG gr
>     then mapM (P.mapM (\c -> spawnCommand c >>= waitForProcess)) cmdsByLevel >> return ()
>     else error "Graph must be one and only one DAG."

> test =
>   let ls = [["sleep 1; echo 1", "sleep 1; echo 2", "abc","sleep 1; echo 4"],
>             ["sleep 1; echo 5", "sleep 1; echo 6", "sleep 1; echo 7","sleep 1; echo 8"], 
>             ["sleep 1; echo 9", "sleep 1; echo 10", "sleep 1; echo 11","sleep 1; echo 12"]]
>   in mapM (P.mapM (\c -> spawnCommand c >>= waitForProcess)) ls


Tests of exec:
*Dag> exec notConnected [1,4] ["echo hello", "echo this", "echo is a", "echo pen"]
*** Exception: Graph must be one and only one DAG.

*Dag> exec cyc [1] ["echo hello", "echo world", "ls"]
*** Exception: Graph must be one and only one DAG.

*Dag> exec mroots [1,7] ["echo hello", "ls", "sleep 3", "echo this", "echo is a", "echo pen", "echo world"]
hello
world
DagTest.hs    Main.hs        Parser.hs    dag.lhs        test.json
this
is a
pen


*Dag> mapM spawnCommand ["echo this", "echo is", "ls", "echo a pen"]
*Dag> is
this
a pen
DagTest.hs    dag.lhs

*Dag> mapM spawnCommand ["echo hello", "echo world"] >>= mapM waitForProcess
hello
world
[ExitSuccess,ExitSuccess]

*Dag> createProcess (proc "echo" ["this", "is", "a", "pen"])
*Dag> this is a pen

*Dag> mapM runCommand ["echo this", "echo is a", "echo pen"]
*Dag> is a
this
pen

*Dag> callCommand "ls"
DagTest.hs    dag.lhs
*Dag> callCommand "echo this is a pen"
this is a pen

----------------------------------------------------------------------------------------
first check the graph has only one connected component
----------------------------------------------------------------------------------------

> isConnected   :: Graph -> Bool 
> isConnected gr = (length . components) gr == 1


-- components :: Graph -> Data.Tree.Forest Vertex
*Dag> components notConnected
[Node {rootLabel = 1, subForest = [Node {rootLabel = 2, subForest = [Node {rootLabel = 3, subForest = []}]}]},Node {rootLabel = 4, subForest = []}]

----------------------------------------------------------------------------------------
second check if the graph is a DAG (detect cycle in a directed graph)
----------------------------------------------------------------------------------------

> isDAG      :: Graph -> Bool
> isDAG graph = (not . and . map isInAnyCycle . assocs) graph
>   where
>     isInAnyCycle     :: (Vertex, [Vertex]) -> Bool 
>     isInAnyCycle (v, vs) = (elem v . concatMap (reachable graph)) vs



-- assocs :: Ix i => Array i e -> [(i, e)]
                                       concatMap (reachable graph) [Vertex]
assocs找到从每个点 v 出发，直接能到的点。再对这些点依次找到从这些点直接和间接能到的点，如果里面有点 v，说明有cycle.
assocs :: Ix i => Array i e -> [(i, e)]
The list of associations of an array in index order.
reachable :: Graph -> Vertex -> [Vertex]
*Dag> reachable line 1
[1,2,3]
*Dag> reachable dag 2
[2,3]
*Dag> reachable notConnected 4
[4]


----------------------------------------------------------------------------------------
third, order of execution
----------------------------------------------------------------------------------------

> nodesByMaxLevel   :: Graph -> [Vertex] -> [[Vertex]]
> nodesByMaxLevel gr = foldr findMax [] . map nub . nodesByLevel gr
>   where
>     findMax       :: [Vertex] -> [[Vertex]] -> [[Vertex]]
>     findMax vs vss = filter (\v -> (and . map (notElem v)) vss) vs : vss

-- foldr :: (a -> b -> b) -> b -> [a] -> b

> nodesByLevel    :: Graph -> [Vertex] -> [[Vertex]]
> nodesByLevel gr  = takeWhile (not.null) . iterate (concatMap (gr!))  

filter vss 里的每个list vs，map 检查 element 的function to each vs in vss, 
vs 里有任何一个元素 v 在任何一个list vs里出现过，就返回False, 不会保留在vs中

(!) :: Ix i => Array i e -> i -> e    
the value at the given index in an array

*Main> mroots ! 2
[5,4,3]

*Main> nodesByMaxLevel mroots [1,7]
[[1,7],[2],[4,3],[5],[6]]

*Main> nodesByLevel mroots [1,7]
[[1,7],[2,4],[5,4,3,5],[6,5,5,6],[6,6]]

*Main> and [True,True,True]
True
*Main> and [False,False,False]
False
*Main> and [True,False,True]
False

*Main> [1] : []
[[1]]
*Main> [1] : [[]]
[[1],[]]
*Main> 1 : []
[1]

> commandsByMaxLevel :: CommandList -> [[Vertex]] -> [CommandList]
> commandsByMaxLevel cmds  = map $ map (\idx -> cmds !! (idx-1))


-- nodes starts from 1

利用了 map 会把 function 的返回值串连成一个 list 的特性

map (map (cmds!!))

*Dag> commandsByMaxLevel ["echo hello", "echo world", "ls", "echo pen"] [[2,4],[3,1]]
[["echo world","echo pen"],["ls","echo hello"]]

commandsByMaxLevel cmds = map $ foldr (\idx accum -> cmds !! (idx-1) : accum) []
map 把 foldr map 到each vs in vss, foldr (...) [] vs means for each v in vs, find the corresponding element in cmdls and form a list of commands, 即 CommandList. 因为 map :: (a -> b) -> [a] -> [b]，这里 foldr 返回的 b 是 CommandList, 经过map 之后就把所有的 CommandList 放到一个 list 里，得到 [CommandList].
spawnCommand is asynchronous, it creates a new process to run the specified shell command. It does not wait for the program to finish, but returns the ProcessHandle.

----------------------------------------------------------------------------------------
third, order of execution
----------------------------------------------------------------------------------------
TODO: order of execution - callCommand, concurrency

> exec1 :: CommandList -> [[Vertex]] -> IO ()
> exec1 cmds vss =
>   let
>     cmdsByLevel :: [CommandList]
>     cmdsByLevel = commandsByMaxLevel cmds vss
>   in
>     mapM (mapM (\c -> spawnCommand c >>= waitForProcess)) cmdsByLevel >> return ()

> nodesByMaxLevel1 :: [[Vertex]] -> [[Vertex]]
> nodesByMaxLevel1  = snd . foldr foldLevel (Set.empty,[])
> foldLevel         :: [Vertex] -> (Set.Set Vertex,[[Vertex]]) -> (Set.Set Vertex,[[Vertex]])
> foldLevel vs (s,r) = let newVertices = filter (\v -> not (Set.member v s)) vs in
>                       (Set.union s (Set.fromList newVertices),newVertices:r)

*Dag> nodesByMaxLevel1 $ nodesByLevel mroots [1,7]
[[1,7],[2],[4,3],[5,5],[6,6]]
