module Main where

import Dag (exec)
import Parser (buildGraph)

import qualified Data.ByteString.Lazy as B (getContents)

main :: IO ()
main = B.getContents >>= (\(g,starters,cmds) -> exec g starters cmds) . buildGraph

